export interface ProductSave {
  id?: number
  name: string
  content: string
  price: number
  images: {
    uri: string
  } []
}
