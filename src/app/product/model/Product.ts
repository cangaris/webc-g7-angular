export interface Product {
  id: number
  name: string
  content: string
  price: number
  categories: ProductCategory[]
  images: ProductImage[]
}

interface ProductCategory {
  id: number
  name: string
}

interface ProductImage {
  id: number
  uri: string
}
