import { NgModule } from '@angular/core';
import { ProductsComponent } from './products/products.component';
import { ProductComponent } from './product/product.component';
import { ProductService } from "./services/product.service";
import { SharedModule } from "../shared/shared.module";
import { AddProductComponent } from './add-product/add-product.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { SaveProductFormComponent } from './save-product-form/save-product-form.component';
import { ProductNameComponent } from './save-product-form/product-name/product-name.component';
import { ProductContentComponent } from './save-product-form/product-content/product-content.component';
import { ProductPriceComponent } from './save-product-form/product-price/product-price.component';
import { ProductImagesComponent } from './save-product-form/product-images/product-images.component';

@NgModule({
  declarations: [
    ProductsComponent,
    ProductComponent,
    AddProductComponent,
    EditProductComponent,
    SaveProductFormComponent,
    ProductNameComponent,
    ProductContentComponent,
    ProductPriceComponent,
    ProductImagesComponent
  ],
  imports: [SharedModule],
  providers: [ProductService]
})
export class ProductModule {
}
