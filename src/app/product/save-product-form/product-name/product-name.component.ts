import { Component } from '@angular/core';
import { SaveProductFormService } from "../save-product-form.service";

@Component({
  selector: 'app-product-name',
  templateUrl: './product-name.component.html',
  styleUrl: './product-name.component.css'
})
export class ProductNameComponent {

  readonly name = this.service.name;

  constructor(private service: SaveProductFormService) {
  }
}
