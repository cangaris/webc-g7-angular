import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SaveProductFormService } from "./save-product-form.service";
import { ProductService } from "../services/product.service";
import { ProductSave } from "../model/ProductSave";
import { Product } from "../model/Product";

@Component({
  selector: 'app-save-product-form',
  templateUrl: './save-product-form.component.html',
  styleUrl: './save-product-form.component.css',
  providers: [SaveProductFormService]
})
export class SaveProductFormComponent implements OnInit {

  readonly form = this.formService.form;
  @Input() product: Product | undefined;
  @Output() productSaved = new EventEmitter<void>();

  constructor(private formService: SaveProductFormService, private productService: ProductService) {
  }

  ngOnInit(): void {
    if (this.product) {
      this.formService.clearImages();
      this.product.images.forEach(() => this.formService.addImage())
      this.form.patchValue(this.product);
    }
  }

  onSubmit(): void {
    if (this.form.valid) {
      this.productService.saveProduct(this.form.value as ProductSave).subscribe(() => {
        this.productSaved.emit();
        this.form.reset();
      });
    } else {
      this.form.markAllAsTouched();
    }
  }
}
