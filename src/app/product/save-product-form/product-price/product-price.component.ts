import { Component } from '@angular/core';
import { SaveProductFormService } from "../save-product-form.service";

@Component({
  selector: 'app-product-price',
  templateUrl: './product-price.component.html',
  styleUrl: './product-price.component.css'
})
export class ProductPriceComponent {

  readonly price = this.service.price;

  constructor(private service: SaveProductFormService) {
  }
}
