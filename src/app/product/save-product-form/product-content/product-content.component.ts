import { Component } from '@angular/core';
import { SaveProductFormService } from "../save-product-form.service";

@Component({
  selector: 'app-product-content',
  templateUrl: './product-content.component.html',
  styleUrl: './product-content.component.css'
})
export class ProductContentComponent {

  readonly content = this.service.content;

  constructor(private service: SaveProductFormService) {
  }
}
