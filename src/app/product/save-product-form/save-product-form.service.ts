import { Injectable } from '@angular/core';
import { FormBuilder, Validators } from "@angular/forms";

const commonValidation = [Validators.required, Validators.minLength(5)]
const imageValidation = [...commonValidation, Validators.minLength(10), Validators.maxLength(255)]
type NullableNumber = number | null;

@Injectable()
export class SaveProductFormService {

  constructor(private fb: FormBuilder) {
  }

  readonly form = this.fb.group({
    id: null as NullableNumber,
    name: ['', [...commonValidation, Validators.maxLength(100)]],
    content: ['', [...commonValidation, Validators.maxLength(65_535)]],
    price: [null as NullableNumber, [Validators.required, Validators.min(1), Validators.max(1_000_000)]],
    images: this.fb.array([
      this.fb.group({
        uri: ['', imageValidation]
      }),
    ])
  });

  readonly images = this.form.controls.images;
  readonly content = this.form.controls.content;
  readonly name = this.form.controls.name;
  readonly price = this.form.controls.price;

  removeImage(index: number): void {
    this.images.removeAt(index);
  }

  addImage(): void {
    this.images.push(
      this.fb.group({
        uri: ['', imageValidation]
      })
    )
  }

  clearImages() {
    this.images.clear();
  }
}
