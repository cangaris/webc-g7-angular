import { Component } from '@angular/core';
import { SaveProductFormService } from "../save-product-form.service";

@Component({
  selector: 'app-product-images',
  templateUrl: './product-images.component.html',
  styleUrl: './product-images.component.css'
})
export class ProductImagesComponent {

  readonly images = this.service.images;

  constructor(private service: SaveProductFormService) {
  }

  addImage(): void {
    this.service.addImage();
  }

  removeImage(index: number): void {
    if (this.service.images.length > 1) {
      this.service.removeImage(index);
    }
  }
}
