import { inject } from "@angular/core";
import { Router } from "@angular/router";
import { RouterPathEnum } from "../../common/RouterPathEnum";

export abstract class CommonSaveProduct {

  readonly router = inject(Router);

  redirect() {
    void this.router.navigateByUrl(RouterPathEnum.products);
  }
}
