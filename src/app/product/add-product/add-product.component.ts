import { Component } from '@angular/core';
import { CommonSaveProduct } from "../common/CommonSaveProduct";

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrl: './add-product.component.css'
})
export class AddProductComponent extends CommonSaveProduct {
}
