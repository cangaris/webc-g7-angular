import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Product } from "../model/Product";
import { Observable } from "rxjs";
import { httpConfig } from "../../common/Common";
import { ProductSave } from "../model/ProductSave";
import { DatabaseId } from "../../common/DatabaseId";

@Injectable()
export class ProductService {

  private static readonly URI = "http://localhost:8080/product";

  constructor(private http: HttpClient) {
  }

  getProduct(id: number): Observable<Product> {
    return this.http.get<Product>(`${ProductService.URI}/${id}`, httpConfig)
  }

  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(ProductService.URI, httpConfig)
  }

  saveProduct(productSave: ProductSave): Observable<undefined | DatabaseId> {
    if (productSave.id) {
      return this.http.put<undefined>(`${ProductService.URI}/${productSave.id}`, productSave, httpConfig);
    }
    return this.http.post<DatabaseId>(ProductService.URI, productSave, httpConfig);
  }

  deleteProduct(id: number): Observable<void> {
    return this.http.delete<void>(`${ProductService.URI}/${id}`, httpConfig)
  }
}
