import { Component } from '@angular/core';
import { ProductService } from "../services/product.service";
import { Observable } from "rxjs";
import { Product } from "../model/Product";
import { RoleEnum } from "../../common/RoleEnum";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrl: './products.component.css',
})
export class ProductsComponent {

  readonly Roles = RoleEnum;
  products$: Observable<Product[]> = this.service.getProducts();

  constructor(private service: ProductService) {
  }

  removeProduct(productId: number) {
    if (!confirm("Czy na pewno usunąć element?")) {
      return;
    }
    this.service.deleteProduct(productId).subscribe(() => this.refreshProducts())
  }

  private refreshProducts() {
    this.products$ = this.service.getProducts()
  }
}
