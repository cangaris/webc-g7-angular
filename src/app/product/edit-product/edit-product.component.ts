import { Component } from '@angular/core';
import { CommonSaveProduct } from "../common/CommonSaveProduct";
import { filter, map, Observable, switchMap } from "rxjs";
import { Product } from "../model/Product";
import { ActivatedRoute } from "@angular/router";
import { ProductService } from "../services/product.service";

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrl: './edit-product.component.css'
})
export class EditProductComponent extends CommonSaveProduct {

  readonly product$: Observable<Product> = this.activatedRoute.paramMap
    .pipe( // Observable<ParamMap>
      filter(paramMap => paramMap.has('id')),
      map(paramMap => paramMap.get('id') as string),
      switchMap(id => this.service.getProduct(+id))
    );

  constructor(private activatedRoute: ActivatedRoute, private service: ProductService) {
    super();
  }
}
