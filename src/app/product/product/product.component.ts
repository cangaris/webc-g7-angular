import { Component } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { ProductService } from "../services/product.service";
import { filter, map, Observable, switchMap } from "rxjs";
import { Product } from "../model/Product";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrl: './product.component.css',
})
export class ProductComponent {

  readonly product$: Observable<Product> = this.activatedRoute.paramMap
    .pipe( // Observable<ParamMap>
      filter(paramMap => paramMap.has('id')),
      map(paramMap => paramMap.get('id') as string),
      switchMap(id => this.service.getProduct(+id))
    );

  constructor(private service: ProductService, private activatedRoute: ActivatedRoute) {
  }
}
