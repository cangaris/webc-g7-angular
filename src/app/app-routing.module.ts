import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductsComponent } from "./product/products/products.component";
import { ProductComponent } from "./product/product/product.component";
import { ProductModule } from "./product/product.module";
import { CategoriesComponent } from "./category/categories/categories.component";
import { CategoryComponent } from "./category/category/category.component";
import { CategoryModule } from "./category/category.module";
import { LoginComponent } from "./user/login/login.component";
import { UserModule } from "./user/user.module";
import { UsersComponent } from "./user/users/users.component";
import { UserComponent } from "./user/user/user.component";
import { EditCategoryComponent } from "./category/edit-category/edit-category.component";
import { AddCategoryComponent } from "./category/add-category/add-category.component";
import { hasAdminOrManagerRoleGuard, hasAdminRoleGuard } from "./shared/has-role.guard";
import { isNotLoggedGuard } from "./shared/is-not-logged.guard";
import { AddProductComponent } from "./product/add-product/add-product.component";
import { EditProductComponent } from "./product/edit-product/edit-product.component";
import { pathId, RouterPathEnum } from "./common/RouterPathEnum";
import { UserEditComponent } from "./user/user-edit/user-edit.component";
import { UserAddComponent } from "./user/user-add/user-add.component";

const routes: Routes = [
  {
    component: ProductsComponent,
    path: RouterPathEnum.products
  },
  {
    component: AddProductComponent,
    path: RouterPathEnum.productAdd,
    canActivate: [hasAdminOrManagerRoleGuard]
  },
  {
    component: EditProductComponent,
    path: RouterPathEnum.productEdit + pathId,
    canActivate: [hasAdminOrManagerRoleGuard]
  },
  {
    component: ProductComponent,
    path: RouterPathEnum.product + pathId
  },
  {
    component: CategoriesComponent,
    path: RouterPathEnum.categories
  },
  {
    component: AddCategoryComponent,
    path: RouterPathEnum.categoryAdd,
    canActivate: [hasAdminOrManagerRoleGuard]
  },
  {
    component: EditCategoryComponent,
    path: RouterPathEnum.categoryEdit + pathId,
    canActivate: [hasAdminOrManagerRoleGuard]
  },
  {
    component: CategoryComponent,
    path: RouterPathEnum.category + pathId
  },
  {
    component: UsersComponent,
    path: RouterPathEnum.users,
    canActivate: [hasAdminOrManagerRoleGuard]
  },
  {
    component: UserAddComponent,
    path: RouterPathEnum.userAdd,
    canActivate: [hasAdminRoleGuard]
  },
  {
    component: UserEditComponent,
    path: RouterPathEnum.userEdit + pathId,
    canActivate: [hasAdminRoleGuard]
  },
  {
    component: UserComponent,
    path: RouterPathEnum.user + pathId,
    canActivate: [hasAdminOrManagerRoleGuard]
  },
  {
    component: LoginComponent,
    path: RouterPathEnum.login,
    canActivate: [isNotLoggedGuard]
  },
  {
    path: "**",
    redirectTo: RouterPathEnum.products
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes), ProductModule, CategoryModule, UserModule],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
