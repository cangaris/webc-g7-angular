export const pathId = "/:id";

export const getRouterPath = (path: RouterPathEnum, withPathId = false) => {
  return `/${path}${withPathId ? pathId : ''}`
}

export enum RouterPathEnum {
  main = "",
  products = "products",
  product = "product",
  productAdd = "product/add",
  productEdit = "product/edit",
  categories = "categories",
  category = "category",
  categoryAdd = "category/add",
  categoryEdit = "category/edit",
  users = "users",
  user = "user",
  userAdd = "user/add",
  userEdit = "user/edit",
  login = "login"
}
