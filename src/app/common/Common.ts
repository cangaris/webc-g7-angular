export const httpConfig = {
  withCredentials: true
}

export const defaultPageSize = 1 as number;
export const defaultPageNumber = 0 as number;
