import { RouterPathEnum } from "../../common/RouterPathEnum";
import { inject } from "@angular/core";
import { Router } from "@angular/router";

export abstract class CommonSaveCategory {

  readonly router = inject(Router);

  redirect(): void {
    void this.router.navigateByUrl(RouterPathEnum.categories);
  }
}
