import { Component } from '@angular/core';
import { CategoryService } from "../services/category.service";
import { Observable } from "rxjs";
import { Category } from "../model/Category";
import { RoleEnum } from "../../common/RoleEnum";

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrl: './categories.component.css'
})
export class CategoriesComponent {

  readonly Roles = RoleEnum;
  categories$: Observable<Category[]> = this.service.getCategories();

  constructor(private service: CategoryService) {
  }

  removeCategory(id: number): void {
    if (!confirm("Czy na pewno usunąć element?")) {
      return;
    }
    this.service.removeCategory(id).subscribe(() => this.refreshCategories());
  }

  private refreshCategories(): void {
    this.categories$ = this.service.getCategories();
  }
}
