import { Component } from '@angular/core';
import { CategoryService } from "../services/category.service";
import { ActivatedRoute } from "@angular/router";
import { filter, map, Observable, switchMap } from "rxjs";
import { Category } from "../model/Category";

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrl: './category.component.css'
})
export class CategoryComponent {

  readonly category$: Observable<Category> = this.activatedRoute.paramMap
    .pipe(
      filter(paramMap => paramMap.has('id')),
      map(paramMap => paramMap.get('id') as string),
      switchMap(id => this.service.getCategory(+id))
    );

  constructor(private service: CategoryService, private activatedRoute: ActivatedRoute) {
  }
}
