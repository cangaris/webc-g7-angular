import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Category } from "../model/Category";
import { httpConfig } from "../../common/Common";
import { DatabaseId } from "../../common/DatabaseId";
import { CategorySave } from "../model/CategorySave";

@Injectable()
export class CategoryService {

  private static readonly URI = 'http://localhost:8080/category';

  constructor(private http: HttpClient) {
  }

  getCategory(id: number): Observable<Category> {
    return this.http.get<Category>(`${CategoryService.URI}/${id}`, httpConfig)
  }

  getCategories(): Observable<Category[]> {
    return this.http.get<Category[]>(CategoryService.URI, httpConfig)
  }

  saveCategory(categorySave: CategorySave): Observable<DatabaseId | undefined> {
    if (categorySave.id) {
      return this.http.put<undefined>(`${CategoryService.URI}/${categorySave.id}`, categorySave, httpConfig)
    }
    return this.http.post<DatabaseId>(CategoryService.URI, categorySave, httpConfig)
  }

  assignProductToCategory(productId: number, categoryId: number): Observable<void> {
    return this.http.post<void>(`${CategoryService.URI}/${categoryId}/product/${productId}`, null, httpConfig)
  }

  removeCategory(id: number): Observable<void> {
    return this.http.delete<void>(`${CategoryService.URI}/${id}`, httpConfig)
  }
}
