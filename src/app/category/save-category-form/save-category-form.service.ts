import { Injectable } from '@angular/core';
import { FormBuilder, Validators } from "@angular/forms";

const commonValidation = [Validators.required, Validators.minLength(5)]

@Injectable()
export class SaveCategoryFormService {

  constructor(private fb: FormBuilder) {
  }

  readonly form = this.fb.group({
    id: null as number | null,
    name: ['', [...commonValidation, Validators.maxLength(100)]],
    content: ['', [...commonValidation, Validators.maxLength(65_535)]],
    image: this.fb.group({
      uri: ['', [...commonValidation, Validators.minLength(10), Validators.maxLength(255)]]
    })
  });

  readonly imageUri = this.form.controls.image.controls.uri;
  readonly content = this.form.controls.content;
  readonly name = this.form.controls.name;
}
