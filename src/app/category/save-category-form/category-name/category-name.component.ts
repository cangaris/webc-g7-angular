import { Component } from '@angular/core';
import { SaveCategoryFormService } from "../save-category-form.service";

@Component({
  selector: 'app-category-name',
  templateUrl: './category-name.component.html',
  styleUrl: './category-name.component.css'
})
export class CategoryNameComponent {
  readonly name = this.service.name;

  constructor(private service: SaveCategoryFormService) {
  }
}
