import { Component } from '@angular/core';
import { SaveCategoryFormService } from "../save-category-form.service";

@Component({
  selector: 'app-category-content',
  templateUrl: './category-content.component.html',
  styleUrl: './category-content.component.css'
})
export class CategoryContentComponent {
  readonly content = this.service.content;

  constructor(private service: SaveCategoryFormService) {
  }
}
