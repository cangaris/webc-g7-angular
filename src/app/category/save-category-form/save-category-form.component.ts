import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CategoryService } from "../services/category.service";
import { CategorySave } from "../model/CategorySave";
import { Category } from "../model/Category";
import { SaveCategoryFormService } from "./save-category-form.service";

@Component({
  selector: 'app-save-category-form',
  templateUrl: './save-category-form.component.html',
  styleUrl: './save-category-form.component.css',
  providers: [SaveCategoryFormService]
})
export class SaveCategoryFormComponent implements OnInit {

  @Input() category: Category | null | undefined;
  @Output() categorySaved = new EventEmitter<void>();

  readonly form = this.saveCategoryFormService.form;

  constructor(
    private saveCategoryFormService: SaveCategoryFormService,
    private categoryService: CategoryService) {
  }

  ngOnInit() {
    if (this.category) {
      this.form.patchValue(this.category);
    }
  }

  onSubmit(): void {
    if (this.form.valid) {
      this.categoryService.saveCategory(this.form.value as CategorySave)
        .subscribe({
          next: () => {
            this.categorySaved.emit();
            this.form.reset();
          }
        })
    } else {
      this.form.markAllAsTouched();
    }
  }
}
