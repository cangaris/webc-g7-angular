import { Component } from '@angular/core';
import { SaveCategoryFormService } from "../save-category-form.service";

@Component({
  selector: 'app-category-image-uri',
  templateUrl: './category-image-uri.component.html',
  styleUrl: './category-image-uri.component.css'
})
export class CategoryImageUriComponent {
  readonly imageUri = this.service.imageUri;

  constructor(private service: SaveCategoryFormService) {
  }
}
