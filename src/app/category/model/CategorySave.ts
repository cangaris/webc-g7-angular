export interface CategorySave {
  id?: number
  name: string
  content: string
  image: { uri: string }
}
