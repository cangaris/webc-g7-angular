export interface Category {
  id: number
  name: string
  content: string
  products: Product[]
  image: Image
}

interface Product {
  id: number
  name: string
}

interface Image {
  id: number
  uri: string
}
