import { NgModule } from '@angular/core';
import { CategoryComponent } from './category/category.component';
import { CategoriesComponent } from './categories/categories.component';
import { CategoryService } from "./services/category.service";
import { SaveCategoryFormComponent } from './save-category-form/save-category-form.component';
import { SharedModule } from "../shared/shared.module";
import { EditCategoryComponent } from './edit-category/edit-category.component';
import { AddCategoryComponent } from './add-category/add-category.component';
import { CategoryNameComponent } from './save-category-form/category-name/category-name.component';
import { CategoryContentComponent } from './save-category-form/category-content/category-content.component';
import { CategoryImageUriComponent } from './save-category-form/category-image-uri/category-image-uri.component';


@NgModule({
  declarations: [
    CategoryComponent,
    CategoriesComponent,
    SaveCategoryFormComponent,
    EditCategoryComponent,
    AddCategoryComponent,
    CategoryNameComponent,
    CategoryContentComponent,
    CategoryImageUriComponent
  ],
  imports: [SharedModule],
  providers: [CategoryService]
})
export class CategoryModule {
}
