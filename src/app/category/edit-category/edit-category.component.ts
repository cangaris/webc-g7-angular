import { Component } from '@angular/core';
import { CategoryService } from "../services/category.service";
import { filter, map, Observable, switchMap } from "rxjs";
import { Category } from "../model/Category";
import { ActivatedRoute } from "@angular/router";
import { CommonSaveCategory } from "../common/CommonSaveCategory";

@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrl: './edit-category.component.css'
})
export class EditCategoryComponent extends CommonSaveCategory {

  readonly category$: Observable<Category> = this.activatedRoute.paramMap
    .pipe(
      filter(paramMap => paramMap.has('id')),
      map(paramMap => paramMap.get('id') as string),
      switchMap(id => this.service.getCategory(+id))
    );

  constructor(private service: CategoryService, private activatedRoute: ActivatedRoute) {
    super();
  }
}
