import { Component } from '@angular/core';
import { CommonSaveCategory } from "../common/CommonSaveCategory";

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrl: './add-category.component.css'
})
export class AddCategoryComponent extends CommonSaveCategory {
}
