import { Component, inject } from '@angular/core';
import { AssignCategoryProductComponent } from "../assign-category-product/assign-category-product.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { RoleEnum } from "../../../common/RoleEnum";

@Component({
  selector: 'app-assign-category-product-btn',
  templateUrl: './assign-category-product-btn.component.html',
  styleUrl: './assign-category-product-btn.component.css'
})
export class AssignCategoryProductBtnComponent {

  readonly RoleEnum = RoleEnum;
  private readonly modal = inject(NgbModal);

  showAssignCategoryProductComponent() {
    this.modal.open(AssignCategoryProductComponent);
  }
}
