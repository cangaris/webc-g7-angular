import { Component, inject } from '@angular/core';
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { FormBuilder, Validators } from "@angular/forms";
import { ProductService } from "../../../product/services/product.service";
import { CategoryService } from "../../../category/services/category.service";

@Component({
  selector: 'app-assign-category-product',
  templateUrl: './assign-category-product.component.html',
  styleUrl: './assign-category-product.component.css'
})
export class AssignCategoryProductComponent {

  private readonly productService = inject(ProductService);
  private readonly categoryService = inject(CategoryService);
  private readonly fb = inject(FormBuilder);
  readonly modal = inject(NgbActiveModal);

  readonly products$ = this.productService.getProducts();
  readonly categories$ = this.categoryService.getCategories();

  readonly form = this.fb.group({
    productId: [null as number | null, [Validators.required]],
    categoryId: [null as number | null, [Validators.required]],
  });

  readonly productId = this.form.controls.productId;
  readonly categoryId = this.form.controls.categoryId;

  assign(): void {
    if (this.form.valid) {
      const productId = this.productId.value as number;
      const categoryId = this.categoryId.value as number;
      this.categoryService.assignProductToCategory(productId, categoryId)
        .subscribe(() => this.modal.close());
    } else {
      this.form.markAllAsTouched();
    }
  }
}
