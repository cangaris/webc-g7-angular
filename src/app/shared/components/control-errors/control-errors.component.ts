import { Component, Input } from '@angular/core';
import { FormControl } from "@angular/forms";

@Component({
  selector: 'app-control-errors',
  templateUrl: './control-errors.component.html',
  styleUrl: './control-errors.component.css'
})
export class ControlErrorsComponent {
  @Input() control!: FormControl;
}
