import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Page } from "../../../common/Page";
import { FormControl } from "@angular/forms";
import { defaultPageSize } from "../../../common/Common";
import { takeUntilDestroyed } from "@angular/core/rxjs-interop";

interface PageAndSize {
  page: number;
  size: number;
}

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrl: './pagination.component.css'
})
export class PaginationComponent {
  @Input() page!: Page<unknown>
  @Output() pageChanged = new EventEmitter<PageAndSize>();
  readonly pageSizeCtrl = new FormControl<number>(defaultPageSize);

  getDataByPageAndSize(page: number, size: number) {
    this.pageChanged.emit({ page, size })
  }

  constructor() {
    this.pageSizeCtrl.valueChanges
      .pipe(takeUntilDestroyed())
      .subscribe((pageNumber) => this.getDataByPageAndSize(pageNumber!, this.page.number))
  }
}
