import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'firstLetterUppercase'
})
export class FirstLetterUppercasePipe implements PipeTransform {

  transform(value: string): string { // zBiGnieW -> Zbigniew
    if (!value) {
      return '';
    }
    return value.at(0)!.toUpperCase() + value.substring(1).toLowerCase();
  }
}
