import { Directive, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';
import { UserService } from "../user/services/user.service";
import { RoleEnum } from "../common/RoleEnum";

@Directive({
  selector: '[appHasRoles]'
})
export class HasRolesDirective implements OnInit {

  @Input() appHasRoles: RoleEnum[] = [];

  constructor(private userService: UserService, private el: ElementRef, private renderer: Renderer2) {
  }

  ngOnInit(): void {
    this.userService.hasRoles(this.appHasRoles)
      .subscribe(hasAccess => {
        if (hasAccess) {
          this.renderer.removeClass(this.el.nativeElement, 'd-none')
        } else {
          this.renderer.addClass(this.el.nativeElement, 'd-none')
        }
      });
  }
}
