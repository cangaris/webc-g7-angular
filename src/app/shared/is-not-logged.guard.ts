import { CanActivateFn } from '@angular/router';
import { UserService } from "../user/services/user.service";
import { inject } from "@angular/core";
import { map } from "rxjs";

export const isNotLoggedGuard: CanActivateFn = () => {
  const userService = inject(UserService)
  return userService.loggedUser$.pipe(map(value => !value));
};
