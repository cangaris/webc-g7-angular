import { CanActivateFn } from '@angular/router';
import { inject } from "@angular/core";
import { UserService } from "../user/services/user.service";
import { RoleEnum } from "../common/RoleEnum";

export const hasAdminOrManagerRoleGuard: CanActivateFn = () => {
  const userService = inject(UserService)
  return userService.hasRoles([RoleEnum.ADMIN, RoleEnum.MANAGER])
};

export const hasAdminRoleGuard: CanActivateFn = () => {
  const userService = inject(UserService)
  return userService.hasRoles([RoleEnum.ADMIN])
};
