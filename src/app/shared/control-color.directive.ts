import { Directive, ElementRef, HostListener, inject, Input, OnInit, Renderer2 } from '@angular/core';
import { FormControl } from "@angular/forms";
import { debounceTime } from "rxjs";

@Directive({
  selector: '[appControlColor]'
})
export class ControlColorDirective implements OnInit {

  private readonly elementRef = inject(ElementRef);
  private readonly renderer = inject(Renderer2);

  @Input() formControl!: FormControl;

  @HostListener('click') onClick(): void {
    this.formControl.markAsTouched();
    this.colorControl()
  }

  ngOnInit(): void {
    this.formControl.valueChanges
      .pipe(debounceTime(100))
      .subscribe(() => this.colorControl())
  }

  private colorControl(): void {
    const control = this.formControl;
    const ref = this.elementRef.nativeElement;
    if (control.touched || control.dirty) {
      if (control.valid) {
        this.renderer.addClass(ref, 'is-valid')
        this.renderer.removeClass(ref, 'is-invalid')
      } else {
        this.renderer.addClass(ref, 'is-invalid')
        this.renderer.removeClass(ref, 'is-valid')
      }
    }
  }
}
