import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterLink } from "@angular/router";
import { HasRolesDirective } from './has-roles.directive';
import { FirstLetterUppercasePipe } from './first-letter-uppercase.pipe';
import { AssignCategoryProductComponent } from './components/assign-category-product/assign-category-product.component';
import {
  AssignCategoryProductBtnComponent
} from './components/assign-category-product-btn/assign-category-product-btn.component';
import { LoaderComponent } from './components/loader/loader.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { NgbPagination } from "@ng-bootstrap/ng-bootstrap";
import { ControlErrorsComponent } from './components/control-errors/control-errors.component';
import { ControlColorDirective } from './control-color.directive';

@NgModule({
  declarations: [
    HasRolesDirective,
    FirstLetterUppercasePipe,
    AssignCategoryProductComponent,
    AssignCategoryProductBtnComponent,
    LoaderComponent,
    PaginationComponent,
    ControlErrorsComponent,
    ControlColorDirective
  ],
  imports: [
    CommonModule,
    RouterLink,
    FormsModule,
    ReactiveFormsModule,
    NgbPagination,
  ],
  exports: [
    CommonModule,
    RouterLink,
    FormsModule,
    ReactiveFormsModule,
    HasRolesDirective,
    FirstLetterUppercasePipe,
    AssignCategoryProductBtnComponent,
    LoaderComponent,
    PaginationComponent,
    ControlErrorsComponent,
    ControlColorDirective
  ],
})
export class SharedModule {
}
