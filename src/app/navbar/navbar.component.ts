import { Component } from '@angular/core';
import { UserService } from "../user/services/user.service";
import { Observable } from "rxjs";
import { Router } from "@angular/router";
import { User } from "../user/model/User";
import { RoleEnum } from "../common/RoleEnum";
import { RouterPathEnum } from "../common/RouterPathEnum";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.css'
})
export class NavbarComponent {

  readonly Roles = RoleEnum;
  readonly loggedUser$: Observable<User | null> = this.userService.loggedUser$;

  constructor(private userService: UserService, private router: Router) {
  }

  logout() {
    this.userService.logout()
      .subscribe(() => {
        this.userService.resetLoggedUser();
        void this.router.navigateByUrl(RouterPathEnum.login);
      })
  }
}
