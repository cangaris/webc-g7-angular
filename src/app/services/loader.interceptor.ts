import { HttpInterceptorFn } from '@angular/common/http';
import { finalize } from "rxjs";
import { inject } from "@angular/core";
import { LoaderService } from "./loader.service";

export const loaderInterceptor: HttpInterceptorFn = (req, next) => {
  const loaderService = inject(LoaderService);
  loaderService.setLoading(true)
  return next(req).pipe(
    finalize(() => loaderService.setLoading(false))
  );
};
