import { HttpErrorResponse, HttpInterceptorFn } from '@angular/common/http';
import { inject } from "@angular/core";
import { Router } from "@angular/router";
import { UserService } from "../user/services/user.service";
import { ToastrService } from "ngx-toastr";
import { catchError, throwError } from "rxjs";
import { RouterPathEnum } from "../common/RouterPathEnum";

export const errorInterceptor: HttpInterceptorFn = (req, next) => {
  const userService = inject(UserService)
  const toastService = inject(ToastrService)
  const router = inject(Router)
  return next(req).pipe(
    catchError((err: HttpErrorResponse) => {
      if (err.status === 401) {
        if (!req.url.endsWith('/user/self')) {
          userService.resetLoggedUser()
          toastService.info("Wymagane logowanie do wykonania tej akcji", "Uwaga!")
          void router.navigateByUrl(RouterPathEnum.login)
        }
      } else if (err.status === 403) {
        toastService.error("Brak uprawnień do danej akcji", "Uwaga!")
        void router.navigateByUrl(RouterPathEnum.main)
      } else if (err.status === 400 || err.status === 409) {
        toastService.error(getMessage(req.url, err.error), "Ups!")
      } else {
        toastService.error("Nieoczekiwany błąd, prosimy o kontakt z administratorem", "Ups!")
      }
      return throwError(() => err);
    })
  );
};

const getMessage = (url: string, error: string[]) => {
  let message = '';
  if (url.endsWith("/login")) {
    message = 'Niepoprawne dane logowania';
  } else {
    message = error.join(', ')
  }
  return message;
}


