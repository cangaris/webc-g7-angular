import { Component } from '@angular/core';
import { CommonUserSaveComponent } from "../common/CommonUserSaveComponent";

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrl: './user-add.component.css'
})
export class UserAddComponent extends CommonUserSaveComponent {
}
