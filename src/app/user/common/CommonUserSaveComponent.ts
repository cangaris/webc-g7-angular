import { inject } from "@angular/core";
import { Router } from "@angular/router";
import { RouterPathEnum } from "../../common/RouterPathEnum";

export abstract class CommonUserSaveComponent {
  readonly router = inject(Router);

  redirect(): void {
    void this.router.navigateByUrl(RouterPathEnum.users);
  }
}
