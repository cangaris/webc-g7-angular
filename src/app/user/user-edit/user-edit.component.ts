import { Component, inject } from '@angular/core';
import { filter, map, Observable, switchMap } from "rxjs";
import { User } from "../model/User";
import { ActivatedRoute } from "@angular/router";
import { UserService } from "../services/user.service";
import { CommonUserSaveComponent } from "../common/CommonUserSaveComponent";

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrl: './user-edit.component.css'
})
export class UserEditComponent extends CommonUserSaveComponent {

  private readonly activatedRoute = inject(ActivatedRoute);
  private readonly service = inject(UserService);

  readonly user$: Observable<User> = this.activatedRoute.paramMap
    .pipe( // Observable<ParamMap>
      filter(paramMap => paramMap.has('id')),
      map(paramMap => paramMap.get('id') as string),
      switchMap(id => this.service.getUser(+id))
    );
}
