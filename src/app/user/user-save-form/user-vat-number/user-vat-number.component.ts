import { Component, inject } from '@angular/core';
import { UserSaveFormService } from "../user-save-form.service";

@Component({
  selector: 'app-user-vat-number',
  templateUrl: './user-vat-number.component.html',
  styleUrl: './user-vat-number.component.css'
})
export class UserVatNumberComponent {
  readonly vatNumber = inject(UserSaveFormService).vatNumber;
}
