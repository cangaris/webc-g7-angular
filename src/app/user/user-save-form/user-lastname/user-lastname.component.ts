import { Component, inject } from '@angular/core';
import { UserSaveFormService } from "../user-save-form.service";

@Component({
  selector: 'app-user-lastname',
  templateUrl: './user-lastname.component.html',
  styleUrl: './user-lastname.component.css'
})
export class UserLastnameComponent {
  readonly lastname = inject(UserSaveFormService).lastname;
}
