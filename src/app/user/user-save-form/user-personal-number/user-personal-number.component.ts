import { Component, inject } from '@angular/core';
import { UserSaveFormService } from "../user-save-form.service";

@Component({
  selector: 'app-user-personal-number',
  templateUrl: './user-personal-number.component.html',
  styleUrl: './user-personal-number.component.css'
})
export class UserPersonalNumberComponent {
  readonly personalNumber = inject(UserSaveFormService).personalNumber;
}
