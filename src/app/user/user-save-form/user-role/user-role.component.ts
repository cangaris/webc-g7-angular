import { Component, inject } from '@angular/core';
import { UserSaveFormService } from "../user-save-form.service";
import { RoleEnum } from "../../../common/RoleEnum";

@Component({
  selector: 'app-user-role',
  templateUrl: './user-role.component.html',
  styleUrl: './user-role.component.css'
})
export class UserRoleComponent {
  readonly Roles = RoleEnum;
  readonly role = inject(UserSaveFormService).role;
}
