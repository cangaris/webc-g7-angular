import { Component, EventEmitter, inject, Input, OnInit, Output } from '@angular/core';
import { User } from "../model/User";
import { UserSaveFormService } from "./user-save-form.service";
import { UserService } from "../services/user.service";
import { UserSave } from "../model/UserSave";

@Component({
  selector: 'app-user-save-form',
  templateUrl: './user-save-form.component.html',
  styleUrl: './user-save-form.component.css',
  providers: [UserSaveFormService]
})
export class UserSaveFormComponent implements OnInit {
  @Input() user: User | undefined;
  @Output() userSaved = new EventEmitter<void>();

  readonly formService = inject(UserSaveFormService);
  readonly userService = inject(UserService);

  readonly form = this.formService.form;

  ngOnInit() {
    if (this.user) {
      this.formService.clearAddresses();
      this.user.address.forEach(() => this.formService.addAddress())
      this.form.patchValue(this.user);
    }
  }

  onSubmit(): void {
    if (this.form.valid) {
      this.userService.saveUser(this.form.value as UserSave)
        .subscribe(() => {
          this.userSaved.emit();
          this.form.reset();
        })
    } else {
      this.form.markAllAsTouched();
    }
  }
}
