import { Component, inject } from '@angular/core';
import { UserSaveFormService } from "../user-save-form.service";

@Component({
  selector: 'app-user-firstname',
  templateUrl: './user-firstname.component.html',
  styleUrl: './user-firstname.component.css'
})
export class UserFirstnameComponent {
  readonly firstname = inject(UserSaveFormService).firstname;
}
