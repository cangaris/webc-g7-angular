import { Component, inject } from '@angular/core';
import { UserSaveFormService } from "../user-save-form.service";

@Component({
  selector: 'app-user-password',
  templateUrl: './user-password.component.html',
  styleUrl: './user-password.component.css'
})
export class UserPasswordComponent {
  readonly password = inject(UserSaveFormService).password;
}
