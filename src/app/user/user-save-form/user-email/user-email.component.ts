import { Component, inject } from '@angular/core';
import { UserSaveFormService } from "../user-save-form.service";

@Component({
  selector: 'app-user-email',
  templateUrl: './user-email.component.html',
  styleUrl: './user-email.component.css'
})
export class UserEmailComponent {
  readonly email = inject(UserSaveFormService).email;
}
