import { Component, inject } from '@angular/core';
import { UserSaveFormService } from "../user-save-form.service";

@Component({
  selector: 'app-user-addresses',
  templateUrl: './user-addresses.component.html',
  styleUrl: './user-addresses.component.css'
})
export class UserAddressesComponent {
  private readonly service = inject(UserSaveFormService);
  readonly addresses = this.service.addresses;

  addressAdd() {
    this.service.addAddress()
  }

  addressRemove(index: number) {
    if (this.addresses.length <= 1) {
      return;
    }
    this.service.removeAddress(index)
  }
}
