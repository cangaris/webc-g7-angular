import { inject, Injectable } from '@angular/core';
import { FormBuilder, Validators } from "@angular/forms";
import { RoleEnum } from "../../common/RoleEnum";

@Injectable()
export class UserSaveFormService {

  private readonly fb = inject(FormBuilder);

  readonly form = this.fb.group({
    id: null as number | null,
    firstname: ['', [Validators.required]],
    lastname: ['', [Validators.required]],
    email: ['', [Validators.required]],
    password: ['', [Validators.required]],
    personalNumber: ['', [Validators.required]],
    vatNumber: ['', [Validators.required]],
    role: [RoleEnum.CLIENT, [Validators.required]],
    address: this.fb.array([
      this.fb.group({
        zipCode: ['', [Validators.required]],
        street: ['', [Validators.required]],
        city: ['', [Validators.required]],
      }),
    ])
  })

  readonly role = this.form.controls.role;
  readonly firstname = this.form.controls.firstname;
  readonly lastname = this.form.controls.lastname;
  readonly email = this.form.controls.email;
  readonly password = this.form.controls.password;
  readonly personalNumber = this.form.controls.personalNumber;
  readonly vatNumber = this.form.controls.vatNumber;
  readonly addresses = this.form.controls.address;

  clearAddresses() {
    this.addresses.clear();
  }

  addAddress() {
    this.addresses.push(
      this.fb.group({
        zipCode: ['', [Validators.required]],
        street: ['', [Validators.required]],
        city: ['', [Validators.required]],
      })
    )
  }

  removeAddress(index: number) {
    this.addresses.removeAt(index);
  }
}
