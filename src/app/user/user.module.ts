import { NgModule } from '@angular/core';
import { LoginComponent } from './login/login.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './user/user.component';
import { SharedModule } from "../shared/shared.module";
import { UserAddComponent } from './user-add/user-add.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import { UserSaveFormComponent } from './user-save-form/user-save-form.component';
import { UserFirstnameComponent } from './user-save-form/user-firstname/user-firstname.component';
import { UserLastnameComponent } from './user-save-form/user-lastname/user-lastname.component';
import { UserEmailComponent } from './user-save-form/user-email/user-email.component';
import { UserPasswordComponent } from './user-save-form/user-password/user-password.component';
import { UserPersonalNumberComponent } from './user-save-form/user-personal-number/user-personal-number.component';
import { UserVatNumberComponent } from './user-save-form/user-vat-number/user-vat-number.component';
import { UserAddressesComponent } from './user-save-form/user-addresses/user-addresses.component';
import { UserRoleComponent } from './user-save-form/user-role/user-role.component';

@NgModule({
  declarations: [
    LoginComponent,
    UsersComponent,
    UserComponent,
    UserAddComponent,
    UserEditComponent,
    UserSaveFormComponent,
    UserFirstnameComponent,
    UserLastnameComponent,
    UserEmailComponent,
    UserPasswordComponent,
    UserPersonalNumberComponent,
    UserVatNumberComponent,
    UserAddressesComponent,
    UserRoleComponent
  ],
  imports: [SharedModule]
})
export class UserModule {
}
