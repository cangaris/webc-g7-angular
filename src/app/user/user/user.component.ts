import { Component } from '@angular/core';
import { filter, map, Observable, switchMap } from "rxjs";
import { ActivatedRoute } from "@angular/router";
import { UserService } from "../services/user.service";
import { User } from "../model/User";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrl: './user.component.css'
})
export class UserComponent {

  readonly user$: Observable<User> = this.activatedRoute.paramMap
    .pipe( // Observable<ParamMap>
      filter(paramMap => paramMap.has('id')),
      map(paramMap => paramMap.get('id') as string),
      switchMap(id => this.service.getUser(+id))
    );

  constructor(private service: UserService, private activatedRoute: ActivatedRoute) {
  }
}
