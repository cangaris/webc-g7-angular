import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { LoginData } from "../model/LoginData";
import { defaultPageNumber, defaultPageSize, httpConfig } from "../../common/Common";
import { BehaviorSubject, map, Observable, switchMap, tap } from "rxjs";
import { User } from "../model/User";
import { Page } from "../../common/Page";
import { RoleEnum } from "../../common/RoleEnum";
import { UserSave } from "../model/UserSave";
import { DatabaseId } from "../../common/DatabaseId";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly loggedUser$$ = new BehaviorSubject<User | null>(null);
  private static readonly URI = 'http://localhost:8080';

  constructor(private http: HttpClient) {
  }

  get loggedUser$(): Observable<User | null> {
    return this.loggedUser$$.asObservable();
  }

  hasRoles(roles: RoleEnum[]): Observable<boolean> {
    return this.loggedUser$.pipe(map(user => !!user && roles.includes(user.role)));
  }

  loginUser(loginData: LoginData): Observable<User> {
    const fd = new FormData();
    fd.append('username', loginData.username)
    fd.append('password', loginData.password)
    fd.append('rememberMe', String(loginData.rememberMe))

    return this.http.post<void>(UserService.URI + '/login', fd, httpConfig)
      .pipe(switchMap(() => this.selfData()));
  }

  resetLoggedUser() {
    this.loggedUser$$.next(null);
  }

  logout() {
    return this.http.post(UserService.URI + '/logout', null, httpConfig);
  }

  selfData(): Observable<User> {
    return this.http.get<User>(UserService.URI + '/user/self', httpConfig)
      .pipe(tap(userData => this.loggedUser$$.next(userData)));
  }

  getUsers(page = defaultPageNumber, size = defaultPageSize): Observable<Page<User>> {
    return this.http.get<Page<User>>(`${UserService.URI}/user`, {
      ...httpConfig,
      params: { page, size },
    })
  }

  getUser(id: number): Observable<User> {
    return this.http.get<User>(`${UserService.URI}/user/${id}`, httpConfig)
  }

  saveUser(user: UserSave): Observable<undefined | DatabaseId> {
    if (user.id) {
      return this.http.put<undefined>(`${UserService.URI}/user/${user.id}`, user, httpConfig)
    }
    return this.http.post<DatabaseId>(`${UserService.URI}/user`, user, httpConfig)
  }

  deleteUser(id: number): Observable<void> {
    return this.http.delete<void>(`${UserService.URI}/user/${id}`, httpConfig)
  }
}
