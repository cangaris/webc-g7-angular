import { Component } from '@angular/core';
import { FormBuilder, Validators } from "@angular/forms";
import { UserService } from "../services/user.service";
import { LoginData } from "../model/LoginData";
import { Router } from "@angular/router";
import { takeUntilDestroyed } from "@angular/core/rxjs-interop";
import { filter } from "rxjs";
import { RouterPathEnum } from "../../common/RouterPathEnum";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {

  // manager@kowalski.pl || admin@kowalski.pl
  // password: dghAk$1
  readonly form = this.fb.group({
    username: ['', [Validators.required, Validators.email, Validators.minLength(5), Validators.maxLength(100)]],
    password: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(100)]],
    rememberMe: false
  });

  readonly username = this.form.controls.username;
  readonly password = this.form.controls.password;

  constructor(private fb: FormBuilder, private service: UserService, private router: Router) {
    this.redirectIfLogged();
  }

  private redirectIfLogged() {
    this.service.loggedUser$
      .pipe(takeUntilDestroyed(), filter(loggedUser => !!loggedUser))
      .subscribe(() => this.router.navigateByUrl(RouterPathEnum.products))
  }

  onSubmit(): void {
    if (this.form.valid) {
      const loginData = this.form.value;
      this.service.loginUser(loginData as LoginData)
        .subscribe({
          error: () => this.service.resetLoggedUser()
        })
    } else {
      this.form.markAllAsTouched();
    }
  }
}
