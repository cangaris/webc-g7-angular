export interface UserSave {
  id?: number
  firstname: string
  lastname: string
  email: string
  password: string
  personalNumber: string
  vatNumber: string
  address: Address[]
}

interface Address {
  zipCode: string
  street: string
  city: string
}
