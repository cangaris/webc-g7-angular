import { RoleEnum } from "../../common/RoleEnum";

export interface User {
  id: number
  firstname: string
  lastname: string
  email: string
  role: RoleEnum
  vatNumber: string
  address: Address[]
}

interface Address {
  zipCode: string
  street: string
  city: string
}
