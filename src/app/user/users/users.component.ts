import { Component } from '@angular/core';
import { UserService } from "../services/user.service";
import { Observable } from "rxjs";
import { User } from "../model/User";
import { RoleEnum } from "../../common/RoleEnum";
import { Page } from "../../common/Page";
import { defaultPageNumber } from "../../common/Common";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrl: './users.component.css'
})
export class UsersComponent {

  readonly Roles = RoleEnum;
  usersPage$: Observable<Page<User>> = this.getUsers();

  constructor(private userService: UserService) {
  }

  removeUser(id: number) {
    if (!confirm("Czy na pewno usunąć element?")) {
      return;
    }
    this.userService.deleteUser(id).subscribe(() => this.refreshUsers());
  }

  getUserByPageAndSize(newPage: number, pageSize: number): void {
    this.usersPage$ = this.userService.getUsers(newPage, pageSize);
  }

  private getUsers() {
    return this.userService.getUsers(defaultPageNumber);
  }

  private refreshUsers() {
    this.usersPage$ = this.getUsers()
  }
}
